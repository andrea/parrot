# Parrot

A React/Firebase expense tracking application, featuring:

- Full CRUD operations on expense records
- Dashboard with expense list, filtering and visualization of expenses on calendar chart
- Firebase Authentication and Firebase/Firestore database

## Requirements

To run the application you will need to:

- [Install Node.js][install node] runtime environment
- [Create a firebase project][create firebase project]

## Quick Start

To run the application...

- Clone the repository
- Log in to your Firebase account and create a Firebase project in the Firebase console.
- Follow [Firebase web setup guide][firebase web setup guide] get your project's initialization informations.
- Open Terminal, head over the project root folder, then rename the file `.env.example` to `.env`
- Edit you `.env` file and fill up the empty keys related to your Firebase project connection info.
- From the project's root folder, install the application by running:

```bash
npm install
```

- Start the client application by running:

```bash
npm start
```

Now head to `http://localhost:3000` and see your running app!

[install node]: https://nodejs.org/en/download/
[create firebase project]: https://firebase.google.com/
[firebase web setup guide]: https://firebase.google.com/docs/web/setup
