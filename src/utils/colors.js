export default {
  text: {
    dark: '#1a1a1a',
    textMuted: '#484848',
    textLight: '#dadada',
  },

  neutral: {
    black: '#000',
    white: '#fff',
    grayDark: '#222',
    gray: '#666',
    grayLight: '#dbdbdb',
  },

  light: {
    surface: '#1a1a1a',
    background: '#fff',

    primary: '#0c1bff',
    primaryDark: '#0c1572',
    primaryLight: '#d9dbf3',

    secondary: '#eea726',
    secondaryDark: '#85360c',
    secondaryLight: '#ffd49b',

    error: '#FF4202',
  },

  dark: {
    surface: '#fff',
    background: '#1a1a1a',

    primary: '#0c1bff',
    primaryDark: '#0c1572',
    primaryLight: '#d9dbf3',

    secondary: '',
    tertiary: '',

    error: '#FF4202',
  },
}
