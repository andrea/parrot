import theme from 'styled-theming'
import { darken, lighten } from 'polished'

import colors from './colors'

export const backgroundColor = theme('palette', {
  light: colors.light.background,
  dark: colors.dark.background,
})

export const color = theme('palette', {
  light: colors.light.surface,
  dark: colors.dark.surface,
})

// Color Names
export const primary = theme('palette', {
  light: colors.light.primary,
  dark: colors.neutral.grayDark,
})

export const primaryHover = theme('palette', {
  light: lighten(0.1, colors.light.primary.toString()),
  dark: lighten(0.1, colors.dark.primary.toString()),
})

export const primaryLight = theme('palette', {
  light: colors.light.primaryLight,
  dark: colors.neutral.gray,
})

export const primaryDark = theme('palette', {
  light: colors.light.primaryDark,
  dark: colors.neutral.gray,
})

export const primaryLightHover = theme('palette', {
  light: lighten(0.1, colors.light.primaryLight.toString()),
  dark: lighten(0.1, colors.dark.primaryLight.toString()),
})

export const secondary = theme('palette', {
  light: colors.light.secondary,
  dark: colors.neutral.grayDark,
})

export const secondaryLight = theme('palette', {
  light: colors.light.secondaryLight,
  dark: colors.neutral.grayDark,
})

export const secondaryDark = theme('palette', {
  light: colors.light.secondaryDark,
  dark: colors.neutral.grayDark,
})

export const white = theme('palette', {
  light: colors.neutral.white,
  dark: colors.neutral.black,
})

export const black = theme('palette', {
  light: colors.neutral.black,
  dark: colors.neutral.white,
})

export const grayDark = theme('palette', {
  light: colors.neutral.grayDark,
  dark: colors.neutral.grayLight,
})

export const grayDarkHover = theme('palette', {
  light: lighten(0.1, colors.neutral.grayDark.toString()),
  dark: darken(0.1, colors.neutral.grayLight.toString()),
})

export const textMuted = theme('palette', {
  light: colors.text.textMuted,
  dark: colors.text.textLight,
})

export const grayLight = theme('palette', {
  light: colors.neutral.grayLight,
  dark: colors.neutral.gray,
})

export const grayLightHover = theme('palette', {
  light: lighten(0.1, colors.neutral.grayLight.toString()),
  dark: lighten(0.1, colors.neutral.grayDark.toString()),
})

export const gray = theme('palette', {
  light: colors.neutral.gray,
  dark: colors.neutral.gray,
})

// Elements
export const buttonColor = theme('palette', {
  light: colors.light.primaryLight,
  dark: colors.neutral.grayLight,
})

export const buttonBackground = theme('palette', {
  light: colors.light.primary,
  dark: colors.neutral.grayDark,
})

export const saveButtonBackground = theme('palette', {
  light: colors.light.primary,
  dark: colors.neutral.gray,
})

export const buttonColorHover = theme('palette', {
  light: colors.neutral.white,
  dark: colors.neutral.white,
})

export const buttonBackgroundHover = theme('palette', {
  light: lighten(0.1, colors.light.primary.toString()),
  dark: lighten(0.1, colors.neutral.grayDark.toString()),
})

export const buttonShadowFocus = theme('palette', {
  light: lighten(0.1, colors.light.primaryLight.toString()),
  dark: lighten(0.1, colors.dark.primaryLight.toString()),
})

export const menuItemColor = theme('palette', {
  light: colors.light.primaryLight,
  dark: colors.neutral.grayLight,
})

export const menuItemColorHover = theme('palette', {
  light: colors.neutral.white,
  dark: colors.neutral.white,
})

export const dateInputFangStroke = theme('palette', {
  light: colors.light.primaryLight,
  dark: colors.neutral.black,
})

export const calendarSelectedColor = theme('palette', {
  light: colors.light.primaryLight,
  dark: colors.light.primaryLight,
})

export const calendarSelectedColorHover = theme('palette', {
  light: colors.light.primaryLight,
  dark: colors.light.primaryLight,
})

export const calendarSelectedBackground = theme('palette', {
  light: colors.light.primary,
  dark: colors.light.primary,
})

export const calendarSelectedBackgroundHover = theme('palette', {
  light: colors.light.primaryDark,
  dark: colors.light.primaryDark,
})
