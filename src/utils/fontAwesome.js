import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faCloud,
  faMoon,
  faPlusCircle,
  faSignOutAlt,
  faSun,
  faTrashAlt,
} from '@fortawesome/free-solid-svg-icons'
import { faGoogle } from '@fortawesome/free-brands-svg-icons'

const icons = [
  faCloud,
  faGoogle,
  faMoon,
  faPlusCircle,
  faSignOutAlt,
  faSun,
  faTrashAlt,
]

export default () => {
  library.add(...icons)
}
