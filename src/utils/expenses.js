import moment from 'moment'

const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate }) =>
  expenses
    .filter(expense => {
      const createdAtMoment = moment(expense.createdAt)
      // prettier-ignore
      const startDateMatch = startDate ? startDate.isSameOrBefore(createdAtMoment, 'day') : true
      // prettier-ignore
      const endDateMatch = endDate ? endDate.isSameOrAfter(createdAtMoment, 'day') : true

      const textMatch = expense.description
        .toLowerCase()
        .includes(text.toLowerCase())

      return startDateMatch && endDateMatch && textMatch
    })
    .sort((a, b) => {
      if (sortBy === 'date') {
        return a.createdAt < b.createdAt ? 1 : -1
      }
      return a.amount < b.amount ? 1 : -1
    })

const totalExpenses = expenses =>
  expenses.map(expense => expense.amount).reduce((sum, val) => sum + val, 0)

export { getVisibleExpenses, totalExpenses }
