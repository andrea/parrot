export const globals = {
  app: {
    name: 'Parrot',
  },
  dashBoard: {
    // Unit of time to define the initial range of expense items to display.
    // You can pick any value allowed by Moment.js
    // http://momentjs.com/docs/#/manipulating/start-of/
    // http://momentjs.com/docs/#/manipulating/end-of/
    unitOfTime: 'month',
  },
}

export const routes = {
  login: {
    path: '/',
  },
  dashboard: {
    path: '/dashboard',
  },
  expenseNew: {
    path: '/new',
  },
  expenseEdit: {
    path: '/edit',
  },
  about: {
    path: '/about',
  },
  notFound: {
    path: '/404',
  },
}
