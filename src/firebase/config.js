import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

// Initialize Firebase
const config = {
  apiKey: process.env.REACT_APP_API_KEY,
  authDomain: process.env.REACT_APP_AUTH_DOMAIN,
  databaseURL: process.env.REACT_APP_DATABASE_URL,
  projectId: process.env.REACT_APP_PROJECT_ID,
  storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
  messagingSenderId: process.env.REACT_APP_MESSAGING_SENDER_ID,
}

firebase.initializeApp(config)

// Initialize Firebase Authentication
const auth = firebase.auth()

// Auth Providers
const googleAuthProvider = new firebase.auth.GoogleAuthProvider()

// Initialize Cloud Firestore through Firebase
const db = firebase.firestore()

// Disable deprecated features
db.settings({
  timestampsInSnapshots: true,
})

export { auth, firebase, db as default, googleAuthProvider }
