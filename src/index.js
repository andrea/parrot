import React from 'react'
import ReactDOM from 'react-dom'
import WebFont from 'webfontloader'
import { Provider } from 'react-redux'
import dotenv from 'dotenv'

import * as serviceWorker from './serviceWorker'
import fontAwesome from './utils/fontAwesome'
import { App, Spinner } from './components'
import store from './store'
import { auth } from './firebase/config'
import { history } from './components/app/app'
import { routes } from './utils/config'
import { login, logout } from './store/actions/auth'
import { startSetExpenses } from './store/actions/expenses'

dotenv.config()

fontAwesome()
WebFont.load({
  google: {
    families: ['Poppins: 500,700', 'Roboto: 400,700'],
  },
})

const parrotApp = (
  <Provider store={store}>
    <App />
  </Provider>
)

let hasRendered = false
const renderApp = () => {
  if (!hasRendered) {
    ReactDOM.render(parrotApp, document.getElementById('root'))
    hasRendered = true
  }
}

ReactDOM.render(<Spinner />, document.getElementById('root'))

auth.onAuthStateChanged(user => {
  if (user) {
    store.dispatch(login(user.uid))
    store.dispatch(startSetExpenses()).then(() => {
      renderApp()
      if (history.location.pathname === routes.login.path) {
        history.push(routes.dashboard.path)
      }
    })
  } else {
    renderApp()
    store.dispatch(logout())
    history.push(routes.login.path)
  }
})

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
