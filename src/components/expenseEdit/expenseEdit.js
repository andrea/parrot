import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import ExpenseForm from '../expenseForm/expenseForm'
// prettier-ignore
import { startEditExpense, startRemoveExpense } from '../../store/actions/expenses'
import { routes } from '../../utils/config'
// prettier-ignore
import { DeleteExpenseButton, DeleteExpenseIcon, ExpenseEditSection, ExpenseEditWrapper, Header, HeaderSection, HeaderWrapper } from './style'

class ExpenseEdit extends Component {
  editExpense = expense => {
    const { id } = this.props.expense
    this.props.startEditExpense({ id, updates: expense })
    this.props.history.push(routes.dashboard.path)
  }

  removeExpense = event => {
    this.props.startRemoveExpense(event)
    this.props.history.push(routes.dashboard.path)
  }

  render() {
    const { expense } = this.props
    if (!expense) {
      return <Redirect to={routes.notFound.path} />
    }

    return (
      <Fragment>
        <HeaderSection>
          <HeaderWrapper>
            <Header>Edit Expense</Header>
            <DeleteExpenseButton
              data-id={expense.id}
              onClick={this.removeExpense}
            >
              <DeleteExpenseIcon icon="trash-alt" />
              Delete
            </DeleteExpenseButton>
          </HeaderWrapper>
        </HeaderSection>
        <ExpenseEditSection>
          <ExpenseEditWrapper>
            <ExpenseForm expense={expense} handleExpense={this.editExpense} />
          </ExpenseEditWrapper>
        </ExpenseEditSection>
      </Fragment>
    )
  }
}

ExpenseEdit.propTypes = {
  expense: PropTypes.object,
  startEditExpense: PropTypes.func,
  startRemoveExpense: PropTypes.func,
  history: PropTypes.object,
  match: PropTypes.object,
}

const mapStateToProps = (state, props) => ({
  expense: state.expenses.find(expense => props.match.params.id === expense.id),
})

const mapDispatchToProps = {
  startEditExpense,
  startRemoveExpense,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpenseEdit)
