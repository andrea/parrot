import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// prettier-ignore
import { grayDark, grayDarkHover, primaryLight, white } from '../../utils/palette'

export const HeaderSection = styled.div`
  background-color: ${primaryLight};
  padding: 2.4rem 0;
`

export const HeaderWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  max-width: 110rem;
  padding: 2.4rem 3.6rem;
  margin: 0 auto;
`

export const Header = styled.h2`
  //text-align: center;
`

export const ExpenseEditSection = styled.div``

export const ExpenseEditWrapper = styled.div`
  max-width: 64rem;
  padding: 2.4rem 3.6rem;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

export const DeleteExpenseButton = styled.button`
  padding: 1.6rem 3.2rem;
  border: none;
  color: ${primaryLight};
  border-radius: 0.4rem;
  font-weight: 700;
  background-color: ${grayDark};
  font-family: Poppins, sans-serif;
  text-decoration: none;

  &:hover {
    cursor: pointer;
    color: ${white};
    background-color: ${grayDarkHover};
  }

  &:focus {
    box-shadow: 0 0 0 4px ${primaryLight};
    outline: none;
  }
`

export const DeleteExpenseIcon = styled(FontAwesomeIcon)`
  margin-right: 0.8rem;
`
