import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { DateRangePicker } from 'react-dates'
import { connect } from 'react-redux'

// prettier-ignore
import { setEndDate, setStartDate, setTextFilter, sortByAmount, sortByDate } from '../../store/actions/filters'
import {
  DateRangeFilter,
  ExpenseListSection,
  ExpenseListWrapper,
  SortByFilter,
  TextFilter,
} from './style'

class ExpenseListFilters extends Component {
  state = {
    searchText: '',
    sortBy: '',
    calendarFocused: null,
  }

  componentDidMount() {
    this.setState(() => ({ sortBy: this.props.filters.sortBy }))
  }

  handleInputChange = event => {
    event.persist()
    this.setState(
      () => ({ searchText: event.target.value }),
      () => {
        this.props.setTextFilter(event.target.value)
      }
    )
  }

  handleSelectChange = event => {
    event.persist()
    this.setState(
      () => ({ sortBy: event.target.value }),
      () => {
        if (event.target.value === 'date') {
          this.props.sortByDate()
        } else {
          this.props.sortByAmount()
        }
      }
    )
  }

  handleDatesChange = ({ startDate, endDate }) => {
    this.props.setStartDate(startDate)
    this.props.setEndDate(endDate)
  }

  handleFocusChange = calendarFocused => {
    this.setState(() => ({ calendarFocused }))
  }

  render() {
    const { calendarFocused, searchText, sortBy } = this.state
    const { filters } = this.props

    return (
      <ExpenseListSection>
        <ExpenseListWrapper>
          <TextFilter>
            <input
              type="text"
              value={searchText}
              placeholder="Search expenses"
              onChange={this.handleInputChange}
            />
          </TextFilter>
          <SortByFilter>
            <select
              name="sortBy"
              id="sortBy"
              onChange={this.handleSelectChange}
              value={sortBy}
            >
              <option value="date">Date</option>
              <option value="amount">Amount</option>
            </select>
          </SortByFilter>
          <DateRangeFilter>
            <DateRangePicker
              startDate={filters.startDate}
              startDateId="date-range-picker-start"
              endDate={filters.endDate}
              endDateId="date-range-picker-end"
              numberOfMonths={1}
              isOutsideRange={() => false}
              onDatesChange={this.handleDatesChange}
              focusedInput={calendarFocused}
              onFocusChange={this.handleFocusChange}
              hideKeyboardShortcutsPanel={true}
            />
          </DateRangeFilter>
        </ExpenseListWrapper>
      </ExpenseListSection>
    )
  }
}

ExpenseListFilters.propTypes = {
  filters: PropTypes.object,
  setTextFilter: PropTypes.func,
  sortByDate: PropTypes.func,
  sortByAmount: PropTypes.func,
  setStartDate: PropTypes.func,
  setEndDate: PropTypes.func,
}

const mapStateToProps = state => ({
  filters: state.filters,
})

const mapDispatchToProps = {
  setTextFilter,
  sortByDate,
  sortByAmount,
  setStartDate,
  setEndDate,
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpenseListFilters)
