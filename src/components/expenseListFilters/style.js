import styled from 'styled-components'

// prettier-ignore
import { calendarSelectedBackground, calendarSelectedBackgroundHover, calendarSelectedColor, calendarSelectedColorHover, dateInputFangStroke, grayDark, grayLight, primary, primaryLight, textMuted, white } from '../../utils/palette'

export const ExpenseListSection = styled.div``

export const ExpenseListWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  max-width: 110rem;
  margin: 0 auto;
  padding: 2.4rem 3.6rem;

  @media screen and (min-width: 800px) {
    flex-direction: row;
  }
`

export const TextFilter = styled.div`
  display: flex;
  font-size: 2rem;
  margin-bottom: 1.6rem;
  border: 1px solid ${primaryLight};
  border-radius: 4px;
  height: 58px;
  width: 100%;

  @media screen and (min-width: 800px) {
    margin: 0 0.8rem 0 0;
  }

  & input {
    border: none;
    border-radius: 4px;
    color: ${textMuted};
    padding: 0.8rem;
    border-bottom: 2px transparent;
    background-color: transparent;
    outline: none;
    width: 100%;

    &:focus {
      box-shadow: 0 0 0 4px ${primaryLight};
      outline: none;
    }
  }
`

export const SortByFilter = styled.div`
  display: flex;
  margin-bottom: 1.6rem;
  width: 100%;

  @media screen and (min-width: 800px) {
    margin: 0 0.8rem;
  }

  & select {
    height: 58px;
    color: ${textMuted};
    font-size: 2rem;
    border: 1px solid ${grayLight};
    border-radius: 4px;
    background: none;
    width: 100%;
    outline: none;

    &:focus {
      box-shadow: 0 0 0 4px ${primaryLight};
      outline: none;
    }
  }
`

export const DateRangeFilter = styled.div`
  display: flex;
  width: 100%;

  @media screen and (min-width: 800px) {
    margin: 0 0 0 0.8rem;
  }

  & .DateInput {
    width: 13rem;
    background-color: transparent;
  }

  & .DateInput_input {
    background-color: transparent;
  }

  & .DateInput_input {
    font-weight: normal;
    font-size: 2rem;
    line-height: 3.6rem;
    color: ${textMuted};
    background-color: transparent;
    width: 100%;
    padding: 0.8rem;
    border: 0;

    border-bottom: 4px solid transparent;
  }

  & .DateRangePicker {
    width: 100%;
  }

  & .DateRangePicker_picker {
    background-color: ${white};
  }

  & .DateRangePickerInput {
    background-color: transparent;
    display: flex;
    align-items: center;
  }

  & .DateRangePickerInput__withBorder {
    border-radius: 4px;
    border: 1px solid ${grayLight};
  }

  & .DateInput_input__focused {
    background-color: transparent;
    border-bottom: 4px solid ${primaryLight};
  }

  & .DayPicker__horizontal {
    background: ${white}; // calendar background color
  }

  & .DayPicker_transitionContainer__horizontal {
    background: ${white}; // calendar background color
  }

  & .DayPickerNavigation_button__default {
    background: ${white}; // calendar background color
  }

  & .DateInput_fangShape {
    fill: ${white}; // calendar background color
  }

  & .DateInput_fangStroke {
    stroke: ${dateInputFangStroke};
  }

  & .CalendarMonth {
    background: ${white}; // calendar background color
  }

  & .CalendarMonthGrid {
    background: ${white}; // calendar background color
  }

  & .CalendarDay__selected_span {
    background: ${primaryLight};
    color: ${grayDark};
    border: 1px solid ${white};

    &:hover {
      color: ${primaryLight};
      background: ${primary};
    }
  }

  & .CalendarDay__selected,
  & .CalendarDay__selected:active,
  & .CalendarDay__selected:hover {
    background-color: ${calendarSelectedBackground};
    color: ${calendarSelectedColor};

    &:hover {
      background-color: ${calendarSelectedBackgroundHover};
      color: ${calendarSelectedColorHover};
    }
  }
`
