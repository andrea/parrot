import React from 'react'

import ExpenseList from '../expenseList/expenseList'
import ExpenseListFilters from '../expenseListFilters/expenseListFilters'
import { Summary } from '..'

const Dashboard = () => (
  <div>
    <Summary />
    <ExpenseListFilters />
    <ExpenseList />
  </div>
)

export default Dashboard
