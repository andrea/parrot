import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

// prettier-ignore
import {
  Button,
  Content,
  GoogleIcon,
  Heading,
  ParrotLogo,
  SkewedBackground,
  Text,
} from './style'
import { startLogin } from '../../store/actions/auth'

const Login = props => (
  <Fragment>
    <SkewedBackground>
      <span />
      <span />
      <span />
      <span />
      <span />
      <span />
    </SkewedBackground>
    <Content>
      <ParrotLogo />
      <Heading>Parrot</Heading>
      <Text>It tracks expenses. No&nbsp;kidding.</Text>
      <Button onClick={props.startLogin}>
        <GoogleIcon icon={['fab', 'google']} /> Login with Google
      </Button>
    </Content>
  </Fragment>
)

Login.propTypes = {
  startLogin: PropTypes.func,
}

const mapDispatchToProps = {
  startLogin,
}

export default connect(
  null,
  mapDispatchToProps
)(Login)
