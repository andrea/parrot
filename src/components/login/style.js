import styled from 'styled-components'
import { lighten } from 'polished'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// prettier-ignore
import Parrot from '../parrot/parrot'

export const SkewedBackground = styled.div`
  height: 100%;
  width: 100%;
  overflow: hidden;
  transform: skewY(-12deg);
  transform-origin: 0;
  background: linear-gradient(150deg, #53f 15%, #05d5ff 70%, #a6ffcb 94%);

  & span {
    height: 150px;
    float: left;
    opacity: 0.1;
    transition: 0.8s;
  }

  & span:hover {
    opacity: 0.3;
    transition: 0.8s;
  }

  & :nth-child(1) {
    background: #a1ffc8;
    width: 10%;
  }

  & :nth-child(2) {
    background: #0dcfff;
    width: 90%;
  }

  & :nth-child(3) {
    background: #53f;
    width: 20%;
  }

  & :nth-child(4) {
    background: #a1ffc8;
    width: 80%;
  }

  & :nth-child(5) {
    background: #0dcfff;
    width: 30%;
  }

  & :nth-child(6) {
    background: #a1ffc8;
    width: 70%;
  }
`

export const Content = styled.div`
  color: #222;
  position: absolute;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  top: 50%;
  left: 50%;
  overflow: auto;
  width: 32rem;
  box-shadow: 0 1.6rem 5.2rem rgba(0, 0, 0, 0.16);
  transform: translate(-50%, -50%);
  z-index: 1;
  padding: 2.4rem;

  &:after {
    content: '';
    width: 100%;
    height: 100%;
    display: block;
    background-color: #fff;
    border-radius: 0.8rem;
    position: absolute;
    opacity: 0.24;
    z-index: -1;
  }

  @media screen and (max-width: 480px) {
    width: 24rem;
  }
`

export const ParrotLogo = styled(Parrot)`
  width: 6.4rem;
`

export const Heading = styled.h1`
  letter-spacing: -0.16rem;
  font-family: Poppins, sans-serif;
  font-size: 4.8rem;
  font-weight: 700;
  margin-bottom: 0.8rem;
`

export const Text = styled.p`
  font-size: 1.6rem;
  margin-bottom: 2.4rem;
  text-align: center;
`

export const Button = styled.button`
  padding: 1.6rem 0;
  width: 100%;
  border: none;
  color: #d9dbf3;
  border-radius: 0.4rem;
  font-weight: 700;
  background-color: #0c1bff;
  font-family: Poppins, sans-serif;

  &:hover {
    cursor: pointer;
    color: #fff;
    background-color: ${lighten(0.08, '#0c1bff')};
  }

  &:focus {
    box-shadow: 0 0 0 4px #d9dbf3;
    outline: none;
  }
`

export const GoogleIcon = styled(FontAwesomeIcon)`
  margin-right: 0.8rem;
`
