import React from 'react'

const NotFoundArt = props => (
  <svg
    xmlnsXlink="http://www.w3.org/1999/xlink"
    width={375}
    height={361}
    viewBox="0 0 375 361"
    {...props}
  >
    <defs>
      <linearGradient id="a" x1="75.518%" x2="61.209%" y1="58.15%" y2="19.323%">
        <stop offset="0%" stopColor="#D9DBF3" />
        <stop offset="100%" stopColor="#AFB1C4" />
      </linearGradient>
      <linearGradient id="b" x1="50%" x2="61.069%" y1="7.927%" y2="46.789%">
        <stop offset="0%" stopColor="#C9BC9F" />
        <stop offset="100%" stopColor="#9D8477" />
      </linearGradient>
      <path id="c" d="M0 .377l109.299 21.165.353 156.718L0 144.565z" />
      <linearGradient id="d" x1="83.207%" y1="43.067%" y2="92.564%">
        <stop offset="0%" stopColor="#4A3F22" />
        <stop offset="100%" stopColor="#C4B799" stopOpacity={0} />
      </linearGradient>
      <path id="f" d="M0 0l65.114 28.428v157.325L0 141.342z" />
      <linearGradient id="h" x1="100%" x2="41.407%" y1="33.417%" y2="90.832%">
        <stop offset="0%" stopColor="#4A3F22" />
        <stop offset="100%" stopColor="#C4B799" stopOpacity={0} />
      </linearGradient>
      <linearGradient id="i" x1="0%" x2="36.95%" y1="65.161%" y2="50%">
        <stop offset="0%" stopColor="#DED8C7" />
        <stop offset="100%" stopColor="#BBB095" />
      </linearGradient>
      <path
        id="j"
        d="M33.107 198.242l4.812-6.389a6.392 6.392 0 0 1 10.037-.221 8.506 8.506 0 0 1-.841 11.707l-3.62 3.29a19.35 19.35 0 0 1-13.015 5.03c-.232 0-.46-.02-.68-.06a5.941 5.941 0 0 1-6.8-5.88v-3.24a5.941 5.941 0 0 1 10.107-4.237z"
      />
      <filter
        id="k"
        width="111.2%"
        height="113.4%"
        x="-5.6%"
        y="-6.7%"
        filterUnits="objectBoundingBox"
      >
        <feOffset dy={-3} in="SourceAlpha" result="shadowOffsetInner1" />
        <feComposite
          in="shadowOffsetInner1"
          in2="SourceAlpha"
          k2={-1}
          k3={1}
          operator="arithmetic"
          result="shadowInnerInner1"
        />
        <feColorMatrix
          in="shadowInnerInner1"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.0457710598 0"
        />
      </filter>
      <path
        id="l"
        d="M17.081 34.255C37.69-.703 68.273-8.471 108.831 10.95c11.958 5.45 23.608 16.184 34.952 32.202l-21.482 10.18c-3.948.338-6.818.338-8.612 0-1.793-.337-4.508-1.388-8.146-3.154L17.081 34.255z"
      />
      <filter
        id="m"
        width="107.1%"
        height="117%"
        x="-3.6%"
        y="-8.5%"
        filterUnits="objectBoundingBox"
      >
        <feOffset dx={9} dy={-3} in="SourceAlpha" result="shadowOffsetInner1" />
        <feComposite
          in="shadowOffsetInner1"
          in2="SourceAlpha"
          k2={-1}
          k3={1}
          operator="arithmetic"
          result="shadowInnerInner1"
        />
        <feColorMatrix
          in="shadowInnerInner1"
          values="0 0 0 0 0.849197491 0 0 0 0 0.583194894 0 0 0 0 0.31116132 0 0 0 0.212013134 0"
        />
      </filter>
      <path
        id="n"
        d="M21.617 199.834c-19.066-93.849-19.066-150.92 0-171.21 28.6-30.439 84.53 0 82.883 12.961 4.56 12.962-6.477 95.193-7.49 110.362-1.014 15.17-22.364 13.019-22.364 22.678-6.694-3.708-6.27-7.416-11.57-3.708-5.3 3.708 1.155-16.969 12.424-22.761 1.146-10.801-17.347-38.716-19.956-73.53-4.91 21.514-25.18 100.35-23.944 125.208-4.717 2.085-8.044 2.085-9.983 0z"
      />
      <filter
        id="o"
        width="106.1%"
        height="103.2%"
        x="-3.1%"
        y="-1.6%"
        filterUnits="objectBoundingBox"
      >
        <feOffset
          dx={-6}
          dy={-1}
          in="SourceAlpha"
          result="shadowOffsetInner1"
        />
        <feComposite
          in="shadowOffsetInner1"
          in2="SourceAlpha"
          k2={-1}
          k3={1}
          operator="arithmetic"
          result="shadowInnerInner1"
        />
        <feColorMatrix
          in="shadowInnerInner1"
          values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.0746999547 0"
        />
      </filter>
      <linearGradient id="p" x1="50%" x2="50%" y1="0%" y2="100%">
        <stop offset="0%" stopColor="#D9DBF3" />
        <stop offset="100%" stopColor="#0C1BFF" />
      </linearGradient>
    </defs>
    <g fill="none" fillRule="evenodd">
      <path fill="#D9DBF3" d="M0 0h375v361H0z" />
      <g transform="translate(54 66)">
        <ellipse
          cx={16.31}
          cy={157}
          fill="#0C1BFF"
          opacity={0.6}
          rx={14.31}
          ry={3}
        />
        <ellipse
          cx={213.813}
          cy={228}
          fill="#0C1BFF"
          opacity={0.6}
          rx={5.813}
          ry={3}
        />
        <ellipse
          cx={240.31}
          cy={225}
          fill="#0C1BFF"
          opacity={0.6}
          rx={14.31}
          ry={3}
        />
        <g transform="translate(29)">
          <path
            fill="url(#a)"
            opacity={0.5}
            d="M56.354 153.564h92.075L292 182.5 171.28 293.385 1.245 180.664z"
          />
          <path
            fill="url(#b)"
            d="M78.159.828l108.472 19.164.481 142.006-108.953-16.982z"
            transform="translate(0 4)"
          />
          <path
            fill="#ADA590"
            d="M78.687 4.781L13.573 32.428v144.805l65.114-31.891z"
          />
          <g transform="translate(12.745 32.153)">
            <mask id="e" fill="#fff">
              <use xlinkHref="#c" />
            </mask>
            <use fill="#D1C5A7" xlinkHref="#c" />
            <path
              fill="url(#d)"
              mask="url(#e)"
              opacity={0.134}
              style={{ mixBlendMode: 'multiply' }}
              d="M0 .377l109.299 8.641.353 63.982L0 59.244z"
            />
            <path
              fill="#FFE5A8"
              mask="url(#e)"
              opacity={0.366}
              d="M43.885 7.452h13.248v156.497H43.885z"
            />
          </g>
          <g transform="translate(122.045 24.7)">
            <mask id="g" fill="#fff">
              <use xlinkHref="#f" />
            </mask>
            <use
              fill="#C4B799"
              transform="matrix(-1 0 0 1 65.114 0)"
              xlinkHref="#f"
            />
            <path
              fill="#AD9A8E"
              d="M16.159 151.463v12.907l-5 3v-12.927l-3.494 2.082 5.841-14.466 5.906 7.466-3.253 1.938z"
              mask="url(#g)"
            />
            <path
              fill="#FFE5A8"
              mask="url(#g)"
              opacity={0.248}
              d="M28.981 14.904l11.485-7.028v150.7l-11.485 7.029z"
            />
            <path
              fill="url(#h)"
              mask="url(#g)"
              opacity={0.134}
              transform="matrix(-1 0 0 1 65.114 0)"
              style={{ mixBlendMode: 'multiply' }}
              d="M0 0l65.114 12.664v76.907L0 55.269z"
            />
          </g>
          <path
            fill="#DDD1B2"
            d="M121.796 53.09l64.758-29.24 46.149 37.917-67.119 37.916z"
          />
          <path
            fill="url(#i)"
            d="M13.573 28.153L122.2 49.17l-8.468 60.089L.103 83.737z"
            transform="translate(0 4)"
          />
          <path
            fill="#0C1BFF"
            d="M31.41 212.257c5.799 0 17.885-6.818 17.885-11.236 0-4.418.186-7.87-5.613-7.87-5.8 0-21.682 9.43-21.682 13.849 0 4.418 3.61 5.257 9.41 5.257z"
            opacity={0.391}
          />
          <ellipse
            cx={66.813}
            cy={211}
            fill="#0C1BFF"
            opacity={0.7}
            rx={5.813}
            ry={3}
          />
          <use fill="#FFF" xlinkHref="#j" />
          <use fill="#000" filter="url(#k)" xlinkHref="#j" />
          <use fill="#FFDF7D" xlinkHref="#l" />
          <use fill="#000" filter="url(#m)" xlinkHref="#l" />
          <use fill="#454545" xlinkHref="#n" />
          <use fill="#000" filter="url(#o)" xlinkHref="#n" />
          <path
            fill="#FFF"
            d="M66.999 164a6.498 6.498 0 0 1 6.49 6.174l1.06 21.21a7.56 7.56 0 0 1-7.55 7.938 7.56 7.56 0 0 1-7.55-7.938l1.06-21.21a6.498 6.498 0 0 1 6.49-6.174z"
          />
          <path
            fill="#575757"
            d="M25.357 40.089c2.934-1.393 12.716-1.594 17.988.106-1.078 8.421-2.9 15.396-3.26 18.864-.359 3.468-15.211 1.955-16.04-1.457-.83-3.412-.668-13.395 1.312-17.513zM85.024 46.378c-2.743-1.74-12.634-3.907-18.075-2.862-3.835 9.84-3.252 15.044-3.318 18.53-.066 3.487 16.876 6.023 18.115 2.737 1.239-3.285 4.741-14.076 3.278-18.405z"
          />
          <path
            stroke="#212121"
            d="M79.279 148.651c1.691 0 7.546-.388 12.082 2.459"
            opacity={0.253}
          />
        </g>
        <path
          fill="#FFF"
          d="M208.493 210.686s.423-3.63 5.172-3.63c1.352 0 4.464.647 9.335 1.941-2.803-1.717-4.452-3.37-4.949-4.955-1.778-5.678.607-8.662 4.95-5.541 9.241 6.644 12.374-1.103 15.831 5.54 3.457 6.645 16.972 18.26 12.891 19.85-4.08 1.588-13.328 3.178-17.282 1.588-3.953-1.589-7.189-9.235-11.44-5.412-4.252 3.823-6.314 12.503-12.343 7.489-6.029-5.015-2.165-16.87-2.165-16.87z"
        />
        <path
          fill="#D8D8D8"
          opacity={0.498}
          d="M232.591 221.201l-5.227-8.778 11.743 8.778z"
        />
        <g>
          <path
            fill="#FFF"
            d="M5.97 144.074s-1.308 1.386.285 3.284c.454.54 1.756 1.567 3.906 3.08-1.626-.544-2.84-.65-3.64-.316-2.865 1.194-3.258 3.148-.555 3.836 5.755 1.466 3.71 5.316 7.524 4.47 3.815-.847 12.99.66 12.257-1.504-.734-2.164-3.2-6.393-5.161-7.44-1.961-1.048-6.102.223-6-2.758.102-2.982 2.88-6.717-1.146-7.444-4.027-.728-7.47 4.792-7.47 4.792z"
          />
          <path
            fill="#D8D8D8"
            opacity={0.498}
            d="M18.256 150.179l-5.262.855 7.447 1.748z"
          />
        </g>
      </g>
      <g fill="url(#p)" transform="scale(-1 1) rotate(-26 -25.677 382.184)">
        <rect width={8} height={22} rx={4} />
        <rect
          width={8}
          height={22}
          x={30.326}
          y={12.921}
          rx={4}
          transform="rotate(45 34.326 23.92)"
        />
        <rect
          width={8}
          height={22}
          x={47}
          y={41}
          rx={4}
          transform="rotate(90 51 52)"
        />
      </g>
    </g>
  </svg>
)

export default NotFoundArt
