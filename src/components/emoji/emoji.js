import React from 'react'
import PropTypes from 'prop-types'

const EmojiCharacter = props => (
  <span
    className="emoji"
    role="img"
    aria-label={props.label ? props.label : ''}
    aria-hidden={props.label ? 'false' : 'true'}
  >
    {props.symbol}
  </span>
)
export default EmojiCharacter

EmojiCharacter.propTypes = {
  label: PropTypes.string,
  symbol: PropTypes.string,
}
