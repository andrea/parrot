import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withTheme } from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// prettier-ignore
import { Button, Heading, Nav, Navbar, NavbarWrapper, NavLink } from './style'
import { routes } from '../../utils/config'
import { startLogout } from '../../store/actions/auth'

const AppHeader = props => (
  <NavbarWrapper>
    <Navbar>
      <Heading>
        <NavLink to={routes.dashboard.path} exact>
          Parrot
        </NavLink>
      </Heading>
      <Nav>
        <Button onClick={props.togglePalette}>
          <FontAwesomeIcon
            icon={props.theme.palette === 'light' ? 'moon' : 'sun'}
            size="lg"
          />
        </Button>
        <Button onClick={props.startLogout}>
          <FontAwesomeIcon icon="sign-out-alt" size="lg" />
        </Button>
      </Nav>
    </Navbar>
  </NavbarWrapper>
)

AppHeader.propTypes = {
  startLogout: PropTypes.func,
  togglePalette: PropTypes.func,
  theme: PropTypes.object,
}

const mapDispatchToProps = {
  startLogout,
}

/*
 * connect() implements shouldComponentUpdate by default, assuming that
 * your component will produce the same results given the same props and state.
 * This is a similar concept to React’s PureRenderMixin.
 * Since Header component depends on global state or React “context”,
 * decorating Header with connect() will fail to update, and NavLink won't be
 * able to detect location correctly. As a consequence, the ".active" CSS class
 * set by NavLink component won't be set. As a work around, we pass the
 * "pure: false" option to connect() as suggested:
 * https://github.com/reduxjs/react-redux/blob/master/docs/troubleshooting.md#my-views-arent-updating-when-something-changes-outside-of-redux
 */
export default compose(
  withTheme,
  connect(
    null,
    mapDispatchToProps,
    null,
    { pure: false }
  )
)(AppHeader)
