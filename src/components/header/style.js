import styled from 'styled-components'
import { NavLink as RouterLink } from 'react-router-dom'

// prettier-ignore
import { menuItemColor, menuItemColorHover, primary, primaryLight } from '../../utils/palette'

export const NavbarWrapper = styled.div`
  background-color: ${primary};
`

export const Heading = styled.h1`
  color: ${primaryLight};
  line-height: 6.4rem;
  border-bottom: 4px solid transparent;
`

export const Navbar = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 2.4rem;
  max-width: 110rem;
  margin: 0 auto;
`

export const Nav = styled.nav`
  display: flex;
  align-items: center;
`

export const NavLink = styled(RouterLink)`
  display: block;
  color: ${menuItemColor};
  font-weight: 700;
  line-height: 6.4rem;
  padding: 0 1.6rem;
  text-decoration: none;
  transition: color 0.24s cubic-bezier(0.4, 0, 0.2, 1);
  &:hover,
  &:active {
    color: ${menuItemColorHover};
  }
  &.active {
    color: ${menuItemColorHover};
  }
  &.active span {
    transform: scaleY(1);
    border-bottom: 4px solid ${menuItemColorHover};
  }
`

export const NavIndicator = styled.span`
  display: block;
  transform: scaleY(0);
  transform-origin: bottom;
  border-bottom: 4px solid ${menuItemColor};
  transition: transform 0.24s cubic-bezier(0.4, 0, 0.2, 1);
`

export const Menu = styled.ul`
  display: flex;
  align-items: center;
`

export const Button = styled.button`
  background: none;
  border: none;
  color: ${menuItemColor};
  cursor: pointer;
  outline: none;
  line-height: 6.4rem;
  padding: 0 1.6rem 0 1.6rem;
  border-bottom: 4px solid transparent;
  transition: color 0.2s;
  &:hover,
  &:active {
    color: ${menuItemColorHover};
  }
`
