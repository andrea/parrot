import React, { Component, Fragment } from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import createBrowserHistory from 'history/createBrowserHistory'
import { ThemeProvider } from 'styled-components'
import 'react-dates/initialize'
import 'react-dates/lib/css/_datepicker.css'

// prettier-ignore
import { Dashboard, ExpenseEdit, ExpenseNew, GlobalStyle, Login, NotFound, PrivateRoute, PublicRoute } from '..'
import { routes } from '../../utils/config'

/*
 * Provide deep integration between React Router history and Redux
 * So that the history object can be used outside the context of
 * React Router and support session authentication somewhere else.
 *
 * https://reacttraining.com/react-router/core/api/Router
 * https://github.com/ReactTraining/history
 */
export const history = createBrowserHistory()

class App extends Component {
  state = {
    palette: 'light',
  }

  handleTogglePalette = () => {
    this.setState(() => ({
      palette: this.state.palette === 'light' ? 'dark' : 'light',
    }))
  }

  render() {
    const { palette } = this.state

    return (
      <ThemeProvider theme={{ palette }}>
        <Router history={history}>
          <Fragment>
            <GlobalStyle />
            {/* prettier-ignore */}
            <Switch>
              <PublicRoute path={routes.login.path} exact component={Login} />
              {/* prettier-ignore */}
              <PrivateRoute path={routes.dashboard.path} exact component={Dashboard} togglePalette={this.handleTogglePalette} />
              {/* prettier-ignore */}
              <PrivateRoute path={routes.expenseNew.path} exact component={ExpenseNew} togglePalette={this.handleTogglePalette} />
              {/* prettier-ignore */}
              <PrivateRoute path={`${routes.expenseEdit.path}/:id`} exact component={ExpenseEdit} togglePalette={this.handleTogglePalette} />
              <Route component={NotFound} />
            </Switch>
          </Fragment>
        </Router>
      </ThemeProvider>
    )
  }
}

export default App
