import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { SingleDatePicker } from 'react-dates'

// prettier-ignore
import { DatePickerWrapper, Form, InputGroup, SaveExpenseButton, SaveExpenseIcon, TextAreaGroup } from './style'

class ExpenseForm extends Component {
  state = {
    expense: {
      description: '',
      amount: '',
      note: '',
      createdAt: moment(),
    },
    calendarFocused: false,
    error: [],
  }

  componentDidMount() {
    if (this.props.expense) {
      // eslint-disable-next-line prefer-const
      let { amount, createdAt, ...rest } = this.props.expense
      amount = (amount / 100).toString()
      createdAt = moment(createdAt)
      this.setState(() => ({ expense: { ...rest, amount, createdAt } }))
    }
  }

  handleDescriptionChange = event => {
    const description = event.target.value
    this.setState(prevState => ({
      expense: { ...prevState.expense, description },
    }))
  }

  handleAmountChange = event => {
    const amount = event.target.value
    // eslint-disable-next-line security/detect-unsafe-regex
    if (!amount || amount.match(/^\d+(\.\d{0,2})?$/)) {
      this.setState(prevState => ({
        expense: { ...prevState.expense, amount },
      }))
    }
  }

  handleDateChange = createdAt => {
    if (createdAt) {
      this.setState(prevState => ({
        expense: { ...prevState.expense, createdAt },
      }))
    }
  }

  handleCalendarFocusChange = ({ focused }) => {
    this.setState(() => ({ calendarFocused: focused }))
  }

  handleNoteChange = event => {
    const note = event.target.value
    this.setState(prevState => ({
      expense: { ...prevState.expense, note },
    }))
  }

  handleSubmit = event => {
    event.preventDefault()
    const { expense } = this.state
    if (!expense.description.trim() || !expense.amount) {
      return this.setState(prevState => ({
        error: [
          ...prevState.error,
          {
            name: 'Invalid expense',
            message: 'Please, provide description and amount.',
          },
        ],
      }))
    }
    return this.setState(
      () => ({ error: [] }),
      () => {
        this.props.handleExpense({
          description: expense.description.trim(),
          amount: Number.parseFloat(expense.amount) * 100,
          createdAt: expense.createdAt.valueOf(),
          note: expense.note.trim(),
        })
      }
    )
  }

  render() {
    const { calendarFocused, expense } = this.state

    // TODO: Implement a snackbar component
    return (
      <Form onSubmit={this.handleSubmit}>
        <InputGroup>
          <input
            type="text"
            placeholder="Description"
            autoFocus
            value={expense.description}
            onChange={this.handleDescriptionChange}
          />
        </InputGroup>
        <InputGroup>
          <input
            type="text"
            placeholder="Amount"
            value={expense.amount}
            onChange={this.handleAmountChange}
          />
        </InputGroup>
        <DatePickerWrapper>
          <SingleDatePicker
            date={expense.createdAt}
            onDateChange={this.handleDateChange}
            focused={calendarFocused}
            onFocusChange={this.handleCalendarFocusChange}
            numberOfMonths={1}
            isOutsideRange={() => false}
            id="datePicker"
            hideKeyboardShortcutsPanel={true}
          />
        </DatePickerWrapper>
        <TextAreaGroup>
          <textarea
            placeholder="Add a note for your expense."
            value={expense.note}
            onChange={this.handleNoteChange}
          />
        </TextAreaGroup>
        <SaveExpenseButton type="submit">
          <SaveExpenseIcon icon="cloud" />
          Save Expense
        </SaveExpenseButton>
      </Form>
    )
  }
}

ExpenseForm.propTypes = {
  expense: PropTypes.object,
  handleExpense: PropTypes.func,
}

export default ExpenseForm
