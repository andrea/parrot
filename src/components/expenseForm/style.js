import styled from 'styled-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// prettier-ignore
import { buttonBackgroundHover, buttonColor, buttonColorHover, buttonShadowFocus, calendarSelectedBackground, calendarSelectedBackgroundHover, calendarSelectedColor, calendarSelectedColorHover, dateInputFangStroke, grayDark, grayLight, primary, primaryLight, saveButtonBackground, textMuted, white } from '../../utils/palette'

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 100%;
  padding: 2.4rem 0;

  @media screen and (min-width: 800px) {
    padding: 2.4rem 3.6rem;
  }
`

export const InputGroup = styled.div`
  display: flex;
  font-size: 2rem;
  margin-bottom: 2.4rem;
  border: 1px solid ${grayLight};
  border-radius: 4px;
  height: 58px;
  width: 100%;

  @media screen and (min-width: 800px) {
  }

  & input {
    border: none;
    border-radius: 4px;
    color: ${textMuted};
    padding: 0.8rem;
    border-bottom: 2px transparent;
    background-color: transparent;
    outline: none;
    width: 100%;

    &:focus {
      box-shadow: 0 0 0 4px ${primaryLight};
      outline: none;
    }
  }
`

export const TextAreaGroup = styled.div`
  display: flex;
  font-size: 2rem;
  margin-bottom: 2.4rem;
  border: 1px solid ${grayLight};
  border-radius: 4px;
  width: 100%;

  & textarea {
    border: none;
    color: #484848;
    padding: 0.8rem;
    border-radius: 4px;
    background-color: transparent;
    outline: none;
    width: 100%;
    height: 16rem;

    &:focus {
      box-shadow: 0 0 0 4px ${primaryLight};
      outline: none;
    }
  }
`

export const SaveExpenseButton = styled.button`
  padding: 1.6rem 3.2rem;
  border: none;
  color: ${buttonColor};
  border-radius: 0.4rem;
  font-weight: 700;
  background-color: ${saveButtonBackground};
  font-family: Poppins, sans-serif;
  text-decoration: none;
  width: 100%;

  &:hover {
    cursor: pointer;
    color: ${buttonColorHover};
    background-color: ${buttonBackgroundHover};
  }

  &:focus {
    box-shadow: 0 0 0 4px ${buttonShadowFocus};
    outline: none;
  }
`

export const SaveExpenseIcon = styled(FontAwesomeIcon)`
  margin-right: 0.8rem;
`

export const DatePickerWrapper = styled.div`
  display: flex;
  width: 100%;
  margin-bottom: 2.4rem;

  & .SingleDatePicker {
    width: 100%;
  }

  & .SingleDatePickerInput__withBorder {
    border: 1px solid ${grayLight};
    border-radius: 4px;
  }

  & .SingleDatePicker_picker {
    background-color: ${white};
  }

  & .SingleDatePickerInput {
    width: 100%;
    background: transparent;
  }

  & .DateInput_fangStroke {
    stroke: ${dateInputFangStroke};
  }

  & .DateInput {
    width: 100%;
    background-color: transparent;
  }

  & .DateInput_input {
    background-color: transparent;
  }

  & .DateInput_input {
    font-weight: normal;
    font-size: 2rem;
    line-height: 3.6rem;
    color: ${textMuted};
    background-color: transparent;
    width: 100%;
    padding: 0.8rem;
    border: 0;

    border-bottom: 4px solid transparent;
  }

  & .DateInput_input__focused {
    background-color: transparent;
    border-bottom: 4px solid ${primaryLight};
  }

  & .DayPicker__horizontal {
    background: ${white}; // calendar background color
  }

  & .DayPicker_transitionContainer__horizontal {
    background: ${white}; // calendar background color
  }

  & .DayPickerNavigation_button__default {
    background: ${white}; // calendar background color
  }

  & .DateInput_fangShape {
    fill: ${white}; // calendar background color
  }

  & .DateInput_fangStroke {
    stroke: ${dateInputFangStroke};
  }

  & .CalendarMonth {
    background: ${white}; // calendar background color
  }

  & .CalendarMonthGrid {
    background: ${white}; // calendar background color
  }

  & .CalendarDay__selected_span {
    background: ${primaryLight};
    color: ${grayDark};
    border: 1px solid ${white};

    &:hover {
      color: ${primaryLight};
      background: ${primary};
    }
  }

  & .CalendarDay__selected,
  & .CalendarDay__selected:active,
  & .CalendarDay__selected:hover {
    background-color: ${calendarSelectedBackground};
    color: ${calendarSelectedColor};
    border-color: ${primaryLight};

    &:hover {
      background-color: ${calendarSelectedBackgroundHover};
      color: ${calendarSelectedColorHover};
    }
  }
`
