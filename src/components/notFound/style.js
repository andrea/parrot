import styled from 'styled-components'
import { Link } from 'react-router-dom'

// prettier-ignore
import { black, buttonBackground, buttonColor, buttonShadowFocus, primaryLight, white } from '../../utils/palette'

export const NotFoundWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: ${primaryLight};
`

export const NotFoundContent = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

export const Text = styled.p`
  color: ${white};
  font-family: Poppins, sans-serif;
  font-size: 2.4rem;
  padding: 2.4rem;
`

export const HomeButton = styled(Link)`
  padding: 1.6rem 3.2rem;
  border: none;
  color: ${black};
  border-radius: 0.4rem;
  font-weight: 700;
  background-color: ${white};
  font-family: Poppins, sans-serif;
  text-decoration: none;

  &:hover {
    cursor: pointer;
    color: ${buttonColor};
    background-color: ${buttonBackground};
  }

  &:focus {
    box-shadow: 0 0 0 4px ${buttonShadowFocus};
    outline: none;
  }
`
