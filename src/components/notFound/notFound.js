import React, { Fragment } from 'react'

import { routes } from '../../utils/config'
import { HomeButton, NotFoundContent, NotFoundWrapper, Text } from './style'
import NotFoundArt from '../notFoundArt/notFoundArt'

const NotFound = () => (
  <Fragment>
    <NotFoundWrapper>
      <NotFoundContent>
        <NotFoundArt />
        <Text>Nothing in here.</Text>
        <HomeButton to={routes.dashboard.path}>Go Home</HomeButton>
      </NotFoundContent>
    </NotFoundWrapper>
  </Fragment>
)

export default NotFound
