import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import ExpenseForm from '../expenseForm/expenseForm'
import {
  ExpenseNewSection,
  ExpenseNewWrapper,
  Header,
  HeaderSection,
  HeaderWrapper,
} from './style'
import { startAddExpense } from '../../store/actions/expenses'
import { routes } from '../../utils/config'

class ExpenseNew extends Component {
  addExpense = expense => {
    this.props.startAddExpense(expense)
    this.props.history.push(routes.dashboard.path)
  }

  render() {
    return (
      <Fragment>
        <HeaderSection>
          <HeaderWrapper>
            <Header>Add Expense</Header>
          </HeaderWrapper>
        </HeaderSection>
        <ExpenseNewSection>
          <ExpenseNewWrapper>
            <ExpenseForm handleExpense={this.addExpense} />
          </ExpenseNewWrapper>
        </ExpenseNewSection>
      </Fragment>
    )
  }
}

ExpenseNew.propTypes = {
  startAddExpense: PropTypes.func,
  history: PropTypes.object,
}

const mapDispatchToProps = {
  startAddExpense,
}

export default connect(
  null,
  mapDispatchToProps
)(ExpenseNew)
