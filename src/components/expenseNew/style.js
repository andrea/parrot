import styled from 'styled-components'

import { primaryLight } from '../../utils/palette'

export const HeaderSection = styled.div`
  background-color: ${primaryLight};
  padding: 2.4rem 0;
`

export const ExpenseNewSection = styled.div``

export const HeaderWrapper = styled.div`
  max-width: 110rem;
  padding: 2.4rem 3.6rem;
  margin: 0 auto;
`

export const Header = styled.h2`
  //text-align: center;
`

export const ExpenseNewWrapper = styled.div`
  max-width: 64rem;
  padding: 2.4rem 3.6rem;
  margin: 0 auto;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`
