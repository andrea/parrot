import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import { AppHeader } from '..'
import { routes } from '../../utils/config'

// prettier-ignore
const PrivateRoute = ({ isAuthenticated, component: Component, togglePalette, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated ? (
        <Fragment>
          <AppHeader togglePalette={togglePalette} />
          <Component {...props} />
        </Fragment>
      ) : (
        <Redirect to={routes.login.path} />
      )
    }
  />
)

PrivateRoute.propTypes = {
  isAuthenticated: PropTypes.bool,
}

const mapStateToProps = state => ({
  isAuthenticated: !!state.auth.uid,
})

export default connect(mapStateToProps)(PrivateRoute)
