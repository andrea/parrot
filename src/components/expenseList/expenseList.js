import PropTypes from 'prop-types'
import React from 'react'
import { connect } from 'react-redux'

import { getVisibleExpenses } from '../../utils/expenses'
import ExpenseListItem from '../expenseListItem/expenseListItem'
// prettier-ignore
import { ExpenseListBody, ExpenseListHeader, ExpenseListSection, ExpenseListWrapper, THead } from './style'

const ExpenseList = ({ expenses }) => (
  <ExpenseListSection>
    <ExpenseListWrapper>
      <ExpenseListHeader>
        <THead>Description</THead>
        <THead>Date</THead>
        <THead>Amount</THead>
      </ExpenseListHeader>
      <ExpenseListBody>
        <ul>
          {expenses.map(expense => (
            <ExpenseListItem key={expense.id} {...expense} />
          ))}
        </ul>
      </ExpenseListBody>
    </ExpenseListWrapper>
  </ExpenseListSection>
)

ExpenseList.propTypes = {
  expenses: PropTypes.array,
  filters: PropTypes.object,
}

const mapStateToProps = state => ({
  expenses: getVisibleExpenses(state.expenses, state.filters),
})

export default connect(mapStateToProps)(ExpenseList)
