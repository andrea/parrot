import styled from 'styled-components'
import { gray, grayLight } from '../../utils/palette'

export const ExpenseListSection = styled.div`
  margin-bottom: 2.4rem;
`

export const ExpenseListWrapper = styled.div`
  max-width: 110rem;
  margin: 0 auto;
  padding: 2.4rem 3.6rem;
`

export const ExpenseListHeader = styled.div`
  background-color: ${grayLight};
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom: 1px solid ${gray};
  display: flex;
  align-items: center;
  justify-content: space-between;
`

export const THead = styled.div`
  font-family: Poppins, sans-serif;
  font-size: 1.8rem;
  font-weight: 700;
  padding: 1.6rem;
  flex-basis: 20%;
  &:first-of-type {
    flex-grow: 1;
    width: 100%;
  }

  @media screen and (max-width: 720px) {
    &:nth-of-type(2) {
      display: none;
    }
  }
`

export const ExpenseListBody = styled.div``
