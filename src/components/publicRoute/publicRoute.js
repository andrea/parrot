import React from 'react'
import PropTypes from 'prop-types'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'

import { routes } from '../../utils/config'

// prettier-ignore
const PublicRoute = ({ isAuthenticated, component: Component, togglePalette, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      isAuthenticated ? (
        <Redirect to={routes.dashboard.path} />
      ) : (
          <Component {...props} />
      )
    }
  />
)

PublicRoute.propTypes = {
  isAuthenticated: PropTypes.bool,
}

const mapStateToProps = state => ({
  isAuthenticated: !!state.auth.uid,
})

export default connect(mapStateToProps)(PublicRoute)
