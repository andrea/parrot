import { normalize } from 'polished'
import { createGlobalStyle } from 'styled-components'
import { backgroundColor, color } from '../../utils/palette'

const GlobalStyle = createGlobalStyle`
  ${normalize()}
  
  html {
    box-sizing: border-box;
    font-size: 62.5% /* adjusts sizing so that 1rem equals 10px */
  }

  *, *::before, *::after {
    box-sizing: inherit;
  }

  body, h1, h2, h3, h4, h5, h6, p, ol, ul {
    padding: 0;
    margin: 0;
  }
  
  body > #root {
    height: 100vh;
  }
  
  ol, ul {
    list-style: none;
  }
  
  img {
    height: auto;
    max-width: 100%;
  }
  
  body {
    color: ${color};
    background-color:${backgroundColor};
    font-family: Roboto, sans-serif;
    font-size: 1.6rem;
  }
  
  h1, h2, h3, h4, h5, h6 {
    font-family: Poppins, sans-serif;
    font-weight: 700;
  }
`

export default GlobalStyle
