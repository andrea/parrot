import styled from 'styled-components'
import { Link } from 'react-router-dom'
import {
  black,
  grayLight,
  grayLightHover,
  textMuted,
  white,
} from '../../utils/palette'

export const ExpenseLink = styled(Link)`
  text-decoration: none;

  &:link,
  &:visited {
    color: ${textMuted};
  }
`

export const TRow = styled.li`
  background-color: ${white};
  border-bottom: 1px solid ${grayLight};
  display: flex;
  align-items: center;
  justify-content: space-between;
  transition: all 0.3s;

  &:hover {
    color: ${black};
    background-color: ${grayLightHover};
  }
`

export const TCell = styled.span`
  padding: 1.6rem;
  flex-basis: 20%;
  &:first-of-type {
    flex-grow: 1;
    width: 100%;
  }

  @media screen and (max-width: 720px) {
    &:nth-of-type(2) {
      display: none;
    }
  }
`
