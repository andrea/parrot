import React from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import numeral from 'numeral'

import { routes } from '../../utils/config'
import { ExpenseLink, TCell, TRow } from './style'

// prettier-ignore
const ExpenseListItem = ({ id, description, amount, createdAt }) => (
    <ExpenseLink to={`${routes.expenseEdit.path}/${id}`}>
      <TRow>
        <TCell>{description}</TCell>
        <TCell>{moment(createdAt).format('MMM Do, YYYY')}</TCell>
        <TCell>{numeral(amount / 100).format('$0,0[.]00')}</TCell>
      </TRow>
    </ExpenseLink>
)

ExpenseListItem.propTypes = {
  id: PropTypes.string,
  description: PropTypes.string,
  amount: PropTypes.number,
  createdAt: PropTypes.number,
  removeExpense: PropTypes.func,
}

export default ExpenseListItem
