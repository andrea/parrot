import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

// prettier-ignore
import { buttonBackground, buttonBackgroundHover, buttonColor, buttonColorHover, buttonShadowFocus, primaryLight } from '../../utils/palette'

export const SummarySection = styled.div`
  background-color: ${primaryLight};
`

export const SummaryWrapper = styled.div`
  max-width: 110rem;
  margin: 0 auto;
  padding: 2.4rem 3.6rem;
`

export const Total = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

export const TotalDisplay = styled.p`
  font-size: 2.4rem;
  text-align: center;
  margin-bottom: 2.4rem;
`

export const TotalValue = styled.span`
  font-weight: 700;
`

export const AddExpenseButton = styled(Link)`
  padding: 1.6rem 3.2rem;
  border: none;
  color: ${buttonColor};
  border-radius: 0.4rem;
  font-weight: 700;
  background-color: ${buttonBackground};
  font-family: Poppins, sans-serif;
  text-decoration: none;

  &:hover {
    cursor: pointer;
    color: ${buttonColorHover};
    background-color: ${buttonBackgroundHover};
  }

  &:focus {
    box-shadow: 0 0 0 4px ${buttonShadowFocus};
    outline: none;
  }
`

export const AddExpenseIcon = styled(FontAwesomeIcon)`
  margin-right: 0.8rem;
`

export const CalendarWrapper = styled.div`
  display: none;

  @media screen and (min-width: 640px) {
    display: block;
    height: 20rem;
    padding: 0 4rem;
    margin: 0 auto;
    max-width: 84rem;
  }
`

export const Tooltip = styled.span``
