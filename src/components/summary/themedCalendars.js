import PropTypes from 'prop-types'
import React from 'react'
import moment from 'moment'
import numeral from 'numeral'
import { ResponsiveCalendar } from '@nivo/calendar'
import { Tooltip } from './style'

const DarkCalendar = props => (
  <ResponsiveCalendar
    // from={moment().format('YYYY')}
    from={props.startDate}
    // to={moment().format('YYYY')}
    to={props.endDate}
    data={props.visibleExpenses}
    emptyColor="#222"
    colors={['#61cdbb', '#97e3d5', '#e8c1a0', '#f47560']}
    margin={{
      top: 56,
      right: 24,
      bottom: 48,
      left: 24,
    }}
    yearSpacing={40}
    monthBorderColor="#666"
    monthLegendOffset={10}
    dayBorderWidth={2}
    dayBorderColor="#666"
    legends={[
      {
        anchor: 'bottom-right',
        direction: 'row',
        translateY: 36,
        itemCount: 4,
        itemWidth: 48,
        itemHeight: 24,
        itemDirection: 'top-to-bottom',
      },
    ]}
    tooltip={({ day, value, color }) => (
      <Tooltip style={{ color }}>
        {moment(day).format('MMM Do')}: {numeral(value).format('$0,0[.]00')}
      </Tooltip>
    )}
    theme={{
      tooltip: {
        container: {
          background: '#333',
        },
      },
    }}
  />
)

const LightCalendar = props => (
  <ResponsiveCalendar
    // from={moment().format('YYYY')}
    from={props.startDate}
    // to={moment().format('YYYY')}
    to={props.endDate}
    data={props.visibleExpenses}
    emptyColor="#e8e6f3"
    colors={['#61cdbb', '#97e3d5', '#e8c1a0', '#f47560']}
    margin={{
      top: 56,
      right: 24,
      bottom: 48,
      left: 24,
    }}
    yearSpacing={40}
    monthBorderColor="#f0f3fa"
    monthLegendOffset={10}
    dayBorderWidth={2}
    dayBorderColor="#f0f3fa"
    legends={[
      {
        anchor: 'bottom-right',
        direction: 'row',
        translateY: 36,
        itemCount: 4,
        itemWidth: 48,
        itemHeight: 24,
        itemDirection: 'top-to-bottom',
      },
    ]}
    tooltip={({ day, value, color }) => (
      <Tooltip style={{ color }}>
        {moment(day).format('MMM Do')}: {numeral(value).format('$0,0[.]00')}
      </Tooltip>
    )}
    theme={{
      tooltip: {
        container: {
          background: '#333',
        },
      },
    }}
  />
)

LightCalendar.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  visibleExpenses: PropTypes.array,
}

DarkCalendar.propTypes = {
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  visibleExpenses: PropTypes.array,
}

export { DarkCalendar, LightCalendar }
