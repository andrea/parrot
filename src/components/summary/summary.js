import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { withTheme } from 'styled-components'
import moment from 'moment'
import numeral from 'numeral'
// prettier-ignore
import { AddExpenseButton, AddExpenseIcon, CalendarWrapper, SummarySection, SummaryWrapper, Total, TotalDisplay, TotalValue } from './style'
import { getVisibleExpenses, totalExpenses } from '../../utils/expenses'
import { routes } from '../../utils/config'
import { DarkCalendar, LightCalendar } from './themedCalendars'

// TODO: Debug calendar component. Browser freezes using dynamic dates.
const Summary = props => {
  const formattedTotal = numeral(props.expenseTotal / 100).format('$0,0.00')
  return (
    <SummarySection>
      <SummaryWrapper>
        <Total>
          <TotalDisplay>
            Totaling <TotalValue>{formattedTotal}</TotalValue> for the selected
            period.
          </TotalDisplay>
          <AddExpenseButton to={routes.expenseNew.path}>
            <AddExpenseIcon icon="plus-circle" />
            New Expense
          </AddExpenseButton>
        </Total>
        <CalendarWrapper>
          {props.theme.palette === 'light' ? (
            <LightCalendar
              visibleExpenses={props.visibleExpenses}
              startDate={props.startDate}
              endDate={props.endDate}
            />
          ) : (
            <DarkCalendar
              visibleExpenses={props.visibleExpenses}
              startDate={props.startDate}
              endDate={props.endDate}
            />
          )}
        </CalendarWrapper>
      </SummaryWrapper>
    </SummarySection>
  )
}

Summary.propTypes = {
  expenseTotal: PropTypes.number,
  visibleExpenses: PropTypes.array,
  startDate: PropTypes.string,
  endDate: PropTypes.string,
  theme: PropTypes.object,
}

const mapStateToProps = state => {
  const visibleExpenses = getVisibleExpenses(state.expenses, state.filters)

  return {
    startDate: moment(state.filters.startDate).toISOString(),
    endDate: moment(state.filters.endDate).toISOString(),
    visibleExpenses: visibleExpenses.map(expense => ({
      value: expense.amount / 100,
      day: moment(expense.createdAt).format('YYYY-MM-DD'),
    })),
    expenseTotal: totalExpenses(visibleExpenses),
  }
}

export default compose(
  withTheme,
  connect(mapStateToProps)
)(Summary)
