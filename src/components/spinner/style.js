import styled from 'styled-components'

// eslint-disable-next-line import/prefer-default-export
export const SpinnerWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`
