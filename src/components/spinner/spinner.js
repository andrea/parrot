import React from 'react'
import { HashLoader } from 'react-spinners'

import { SpinnerWrapper } from './style'

const Spinner = () => (
  <SpinnerWrapper>
    <HashLoader sizeUnit="px" size={50} color="#0c1bff" />
  </SpinnerWrapper>
)

export default Spinner
