// prettier-ignore
import { ADD_EXPENSE, EDIT_EXPENSE, REMOVE_EXPENSE, SET_EXPENSES } from '../actions/types'

const initialState = []

const expenses = (state = initialState, action) => {
  switch (action.type) {
    case ADD_EXPENSE:
      return [...state, action.payload]

    case EDIT_EXPENSE:
      return state.map(expense => {
        if (expense.id === action.payload.id) {
          return { ...expense, ...action.payload.updates }
        }
        return expense
      })

    case REMOVE_EXPENSE:
      return state.filter(expense => expense.id !== action.payload.id)

    case SET_EXPENSES:
      return action.payload.expenses

    default:
      return state
  }
}

export default expenses
