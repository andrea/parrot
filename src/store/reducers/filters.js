import moment from 'moment'

// prettier-ignore
import { SET_END_DATE, SET_START_DATE, SET_TEXT_FILTER, SORT_BY_AMOUNT, SORT_BY_DATE } from '../actions/types'
import { globals } from '../../utils/config'

const initialState = {
  text: '',
  sortBy: 'date',
  startDate: moment().startOf(globals.dashBoard.unitOfTime),
  endDate: moment().endOf(globals.dashBoard.unitOfTime),
}

const filters = (state = initialState, action) => {
  switch (action.type) {
    case SET_TEXT_FILTER:
      return {
        ...state,
        text: action.payload.text,
      }

    case SORT_BY_AMOUNT:
      return {
        ...state,
        sortBy: 'amount',
      }

    case SORT_BY_DATE:
      return {
        ...state,
        sortBy: 'date',
      }

    case SET_START_DATE:
      return {
        ...state,
        startDate: action.payload.startDate,
      }

    case SET_END_DATE:
      return {
        ...state,
        endDate: action.payload.endDate,
      }

    default:
      return state
  }
}

export default filters
