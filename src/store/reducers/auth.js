import { LOGIN, LOGOUT } from '../actions/types'

const initialState = {}

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        uid: action.payload.uid,
      }

    case LOGOUT:
      return {}

    default:
      return state
  }
}

export default auth
