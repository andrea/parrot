import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'

import auth from './reducers/auth'
import expenses from './reducers/expenses'
import filters from './reducers/filters'

const store = createStore(
  combineReducers({
    auth,
    expenses,
    filters,
  }),
  composeWithDevTools(applyMiddleware(thunk))
)

export default store
