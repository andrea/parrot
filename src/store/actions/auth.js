import { auth, googleAuthProvider } from '../../firebase/config'
import { LOGIN, LOGOUT } from './types'

export const login = uid => ({
  type: LOGIN,
  payload: {
    uid,
  },
})

export const startLogin = () => () =>
  auth.signInWithRedirect(googleAuthProvider)

export const logout = () => ({
  type: LOGOUT,
})

export const startLogout = () => () => auth.signOut()
