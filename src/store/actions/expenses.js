import db from '../../firebase/config'
// prettier-ignore
import { ADD_EXPENSE, EDIT_EXPENSE, REMOVE_EXPENSE, SET_EXPENSES } from './types'

// ADD_EXPENSE
export const addExpense = expense => ({
  type: ADD_EXPENSE,
  payload: expense,
})

export const startAddExpense = (expenseData = {}) => (dispatch, getState) => {
  const { uid } = getState().auth
  const { description = '', note = '', amount = 0, createdAt = 0 } = expenseData
  const expense = { description, note, amount, createdAt }

  // prettier-ignore
  db.collection(`users/${uid}/expenses`).add(expense)
    .then(docRef => dispatch(addExpense({ id: docRef.id, ...expense })))
}

// EDIT_EXPENSE
export const editExpense = ({ id, updates }) => ({
  type: EDIT_EXPENSE,
  payload: {
    id,
    updates,
  },
})

export const startEditExpense = ({ id, updates }) => (dispatch, getState) => {
  const { uid } = getState().auth
  // prettier-ignore
  db.doc(`/users/${uid}/expenses/${id}`).update(updates)
    .then(() => {
      dispatch(editExpense({ id, updates }))
    })
}

// REMOVE_EXPENSE
export const removeExpense = id => ({
  type: REMOVE_EXPENSE,
  payload: { id },
})

export const startRemoveExpense = event => (dispatch, getState) => {
  const { id } = event.target.dataset
  const { uid } = getState().auth
  // prettier-ignore
  db.doc(`/users/${uid}/expenses/${id}`).delete()
    .then(() => {
      dispatch(removeExpense(id))
    })
}

// SET_EXPENSES
export const setExpenses = expenses => ({
  type: SET_EXPENSES,
  payload: {
    expenses,
  },
})

export const startSetExpenses = () => (dispatch, getState) => {
  const { uid } = getState().auth
  // prettier-ignore
  return db.collection(`/users/${uid}/expenses`).get().then(snapshot => {
    const expenses = []
    snapshot.forEach(doc => expenses.push({ id: doc.id, ...doc.data() }))
    dispatch(setExpenses(expenses))
  })
}
